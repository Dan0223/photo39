package application;
import controller.adminController;
import controller.loginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
public class Photos extends Application {
/**
 * This loads the initial screen where the user can login
 */
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(
					getClass().getResource("/view/login.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			loginController listController = 
					loader.getController();
			listController.start(primaryStage);
			Scene scene = new Scene(root, 600, 500);
			primaryStage.setResizable(false);
			
			primaryStage.setScene(scene);
				
			primaryStage.show(); 

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Launches the application
	 * @param args launches program
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
