package controller;

import java.awt.Container;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.User;
import objects.UserList;

/**
 * Opens a manual slide show that displays the photo and the Date 
 * 
 *
 */
public class slideShowController {
	@FXML 
	ImageView imageview;
	@FXML
	Label captionLabel;
	@FXML
	Label dateLabel;
	@FXML
	Label tagNameLabel;
	@FXML
	Label tagTypeLabel;
	int position;
	String caption;
	String date;
	Album album;
	User user;
	ArrayList<Photo> photoList;
	Image curr;
	/**
	 * Passes in the Album and User from the previous page
	 * @param album passes in the current album
	 * @param user passes in the current user
	 */
	public void start(Album album,User user){
		//NEED TO ADD TAG AND CALENDER DETAILS
		this.album = album;
		this.user = user;
		position = 0; 
		photoList = album.photoList;
	
		curr = new Image("file:///"+photoList.get(position).path);
		imageview.setImage(curr);
		imageview.setPreserveRatio(true);
		
		caption = photoList.get(position).caption;
		
		date = photoList.get(position).d.toString();
		
		
		if(!(caption.equals(null))&&caption != null) {
			captionLabel.setText(caption);
		}
		if(!(date.equals(null))&&date != null) {
			dateLabel.setText(date);
		}
	}
	/**
	 * Button that will allow to go to the next picture
	 */
	public void next() {
	int size = photoList.size();

		if(position + 1 > size-1) {
			return;
		}
		position++;
		curr = new Image("file:///"+photoList.get(position).path);
		imageview.setImage(curr);
		imageview.setPreserveRatio(true);
		caption = photoList.get(position).caption;
		
		if(!(caption.equals(null))&&caption != null) {
			captionLabel.setText(caption);
		}
		
	}
	
	/**
	 * Button that will allow to go to the previous picture
	 */
	public void prev() {
		

		if(position - 1 < 0) {
			return;
		}
		position--;
		
		curr = new Image("file:///"+photoList.get(position).path);
		imageview.setImage(curr);
		imageview.setPreserveRatio(true);
		caption = photoList.get(position).caption;
		
		if(!(caption.equals(null))&&caption != null) {
			captionLabel.setText(caption);
		}
	}
	
	/**
	 * Button that will allow to go back to the previous screen
	 * @param event passes in the new scene
	 * @throws IOException if error caught
	 */
	public void back(ActionEvent event) throws IOException{
		String albumName = album.albumName;
		FXMLLoader loader1 = new FXMLLoader();
		loader1.setLocation(getClass().getResource("/view/albumOpen.fxml"));
		Parent userPage = loader1.load();
		Scene userScene = new Scene(userPage);
		AlbumOpenController controller = loader1.getController();
		controller.start(user.getAlbum(albumName),user);
		Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window1.setScene(userScene);
		window1.show();
		
		
	}
}
