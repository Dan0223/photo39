package controller;

import objects.Album;
import objects.User;
import objects.UserList;
import java.awt.Container;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javafx.event.ActionEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.Tag;
import objects.User;
import objects.UserList;;

/**
 * Allows user to copy on Picture to another album and move one picture to
 * another album
 * 
 *
 */
public class relocateController {
	Album album;
	User user;
	UserList userList;
	private ObservableList<String> obsList;
	ArrayList<String> albumList = new ArrayList<String>();

	@FXML
	ListView<String> albumListView;
	String path = "";

	/**
	 * Initializes user, path, album, and diplays a List view of the user's albums
	 * 
	 * @param album album of the user
	 * @param user  user that is doing command
	 * @param path  path of the photo
	 */
	public void start(Album album, User user, String path) {
		userList = new UserList();
		userList.listInit();
		this.path = path;
		this.album = album;
		this.user = user;
		albumList = user.getAlbumNames();
		obsList = FXCollections.observableArrayList(albumList);
		FXCollections.sort(obsList, String.CASE_INSENSITIVE_ORDER);
		albumListView.setItems(obsList);
		albumListView.getSelectionModel().select(0);
	}

	// add photo to multiple albums
	/**
	 * Copys a picture from one album to another
	 */

	public void copy() {
		String albumName = albumListView.getSelectionModel().getSelectedItem().trim();
		// user.albumList.(albumName).addPhoto(path);
		Photo p1 = album.getPhoto(path);
		Photo p2 = new Photo(p1.path);
		p2.caption = p1.caption;

		p2.tagList = getTags(p1.tagList);
		Album temp1 = user.getAlbum(albumName);

		temp1.addPhoto(p2);
		user.updateAlbum(temp1);
		userList.updateUser(user);
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Confirmation");
		alert.setHeaderText("Information Dialog");
		alert.setContentText("Photo has been copied successfully");

		alert.showAndWait();

	}

	// delete photo from OG album and move to new location
	/**
	 * Moves a photo fromone location to another
	 */
	public void moveTo() {
		String albumName = albumListView.getSelectionModel().getSelectedItem().trim();

		Photo temp = album.getPhoto(path);
		Album temp1 = user.getAlbum(albumName);
		temp1.addPhoto(temp);
		album.deletePhoto(path);
		user.updateAlbum(temp1);
		userList.updateUser(user);
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Confirmation");
		alert.setHeaderText("Information Dialog");
		alert.setContentText("Photo has been moved successfully");

		alert.showAndWait();
	}

	/**
	 * goes back to the Album Open page
	 * 
	 * @param event Event to be handled
	 * @throws IOException if error caught
	 */
	public void back(ActionEvent event) throws IOException {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/albumOpen.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);
		AlbumOpenController controller = loader.getController();
		controller.start(album, user);
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();

	}

	/**
	 * returns a List of tags in a comma separated form
	 * 
	 * @param source ArrayList of tags
	 * @return ArrayList of tags
	 */
	public ArrayList<Tag> getTags(ArrayList<Tag> source) {
		ArrayList<Tag> result = new ArrayList<Tag>();
		for (Tag t : source) {

			result.add(t);
		}
		return result;
	}

}
