package controller;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.Tag;
import objects.User;
/**
 * Searches for photos by date
 * 
 *
 */
public class searchByDateController {
	@FXML
	DatePicker pastDate;
	@FXML
	DatePicker futureDate;
	User user;
	

	public void start(User user) {
		this.user = user;
	

	}
/**
 * Uses Date picker to get two points in time and compares them with the picture's date and time
 * @param event makes new scene
 * @throws IOException if error caught
 */
	public void search(ActionEvent event) throws IOException {
		try {
		LocalDate localPast = pastDate.getValue();
		LocalDate localFuture = futureDate.getValue();
		Instant instantPast = Instant.from(localPast.atStartOfDay(ZoneId.systemDefault()));
		Instant instantFuture = Instant.from(localFuture.atStartOfDay(ZoneId.systemDefault()));
		Date datePast = Date.from(instantPast);
		Date dateFuture = Date.from(instantFuture);
		
		ArrayList<String> albumList = new ArrayList<>();
		ArrayList<String> pathList = new ArrayList<>();
		ArrayList<Photo> photoList = new ArrayList<>();
		albumList = user.getAlbumNames();
		for (String name : albumList) {
			Album curr = user.getAlbum(name);
			for (Photo p : curr.photoList) {
				Date currDate = p.d;
				if (currDate.after(datePast) && currDate.before(dateFuture)) {
					System.out.println("Yea");
					Photo newPhoto = new Photo(p.path);
					newPhoto.caption = p.caption;
					newPhoto.c = p.c;
					newPhoto.d = p.d;
					newPhoto.tagList = getTags(p.tagList);
					pathList.add(p.path);
					photoList.add(p);
				}

			}

		}
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/mainSearch.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);
		mainSearchController controller = loader.getController();
		controller.loadPics(pathList, photoList, user);
	
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
		}catch(NullPointerException e){
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("ERROR");
			alertDel.setHeaderText(null);
			alertDel.setContentText("Error! Missing fields");
			alertDel.showAndWait();
			return;
		}
	}
	/**
	 * Goes back to the search page
	 * @param event Event to be handle
	 * @throws IOException if error caught
	 */
	public void back(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/mainSearch.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);
		mainSearchController controller = loader.getController();
		controller.start(user);
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();

	}
	/**
	 * returns a List of tags in a  comma separated form
	 * @param source ArrayList of tags
	 * @return ArrayList returns an array list of tags
	 */
	public ArrayList<Tag> getTags(ArrayList<Tag> source) {
		ArrayList<Tag> result = new ArrayList<Tag>();
		for (Tag t : source) {

			result.add(t);
		}
		return result;
	}
	
}
