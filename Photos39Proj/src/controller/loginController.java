//Daniel Alvarado
//Al Marique

package controller;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.User;
import objects.UserList;
/**
 * Login page that takes in a user and sends them to the appropriate page
 * 
 *
 */
public class loginController {
	@FXML
	TextField loginBox;

	// This is the List of users from the .ser file
	UserList users = new UserList();
	// private Stage primaryStage;

	public void start(Stage primaryStage) throws Exception {
		// this.primaryStage = primaryStage;

	}
/**Checks to see if a user exists and sends them to the appropriate page
 * 
 * @param event event to handle
 * @throws IOException if error caught
 */
	public void login(ActionEvent event) throws IOException {
		users.listInit();

		String userName = loginBox.getText().trim();

		if (userName.equals("admin")) {

			// go to admin page
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/admin.fxml"));
			Parent adminPage = loader.load();
			Scene adminScene = new Scene(adminPage);
			adminController controller = loader.getController();
			controller.start();
			Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
			window.setScene(adminScene);

			window.show();

		} else if (users.contains(userName)) {

			users.getUser(userName);

			FXMLLoader loader1 = new FXMLLoader();
			loader1.setLocation(getClass().getResource("/view/albumList.fxml"));
			// \Photos39Proj\src\view\albumList.fxml
			Parent userPage = loader1.load();
			Scene userScene = new Scene(userPage);
			userController controller = loader1.getController();
			controller.start(users.getUser(userName));
			Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
			window1.setScene(userScene);
			window1.show();

			// go to user page
		} else if (userName.equals("stock")) {
			String path = "";
			User stock = new User("stock");
			stock.addAlbum("stockAlbum");
			Album stockAlbum = stock.albumList.get(0);
			path = getClass().getResource("/duck.jpg").toString();		
			path = path.replaceAll("file:/","");
			Photo image1 = new Photo(path);
			stockAlbum.addPhoto(image1);			
			
			path = getClass().getResource("/emoji.jpg").toString();		
			path = path.replaceAll("file:/","");
			Photo image2 = new Photo(path);
			stockAlbum.addPhoto(image2);
			
			
			path = getClass().getResource("/face.jpg").toString();		
			path = path.replaceAll("file:/","");
			Photo image3 = new Photo(path);
			
			stockAlbum.addPhoto(image3);
			
			path = getClass().getResource("/flower.jpg").toString();		
			path = path.replaceAll("file:/","");
			Photo image4 = new Photo(path);
			stockAlbum.addPhoto(image4);
			
			path = getClass().getResource("/rose.jpg").toString();		
			path = path.replaceAll("file:/","");
			Photo image5 = new Photo(path);
			stockAlbum.addPhoto(image5);
			
			stock.updateAlbum(stockAlbum);
			users.addUser(stock);
			
			FXMLLoader loader1 = new FXMLLoader();
			loader1.setLocation(getClass().getResource("/view/albumList.fxml"));
			// \Photos39Proj\src\view\albumList.fxml
			Parent userPage = loader1.load();
			Scene userScene = new Scene(userPage);
			userController controller = loader1.getController();
			controller.start(users.getUser(userName));
			Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
			window1.setScene(userScene);
			window1.show();
			
		} else {
			// display an alert saying user does not exist.
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText(null);
			alert.setContentText("User not found!!!");
			alert.showAndWait();
		}
	}

	

}
