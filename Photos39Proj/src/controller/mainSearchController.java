package controller;

import java.awt.desktop.UserSessionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.User;
import objects.UserList;
import java.util.Optional;

/**
 * Displays the results of the search you make with search by tag or date
 * 
 *
 */

public class mainSearchController {
	@FXML
	ListView<String> listview = new ListView<String>();
	ArrayList<String> pictures;
	User user;
	ArrayList<Photo> photosList = new ArrayList<Photo>();
	ObservableList<String> items;
	Image curr;
	ArrayList<String> albumList = new ArrayList<String>();
	UserList userList = new UserList();

	public void start(User user) {
		this.user = user;
		albumList = user.getAlbumNames();
	}

	/**
	 * loads pictures and thumbnail
	 * 
	 * @param pictures list of paths
	 * @param Photos   list of Photos
	 * @param user     current user
	 * 
	 */
	public void loadPics(ArrayList<String> pictures, ArrayList<Photo> Photos, User user) {
		albumList = user.getAlbumNames();
		this.user = user;
		this.photosList = Photos;
		this.pictures = pictures;
		if (pictures.isEmpty() || Photos.isEmpty()) {
			return;
		} else {
			items = FXCollections.observableArrayList(pictures);
			listview.setItems(items);
			listview.setCellFactory(param -> new ListCell<String>() {

				private ImageView imageView = new ImageView();

				@Override
				public void updateItem(String name, boolean empty) {
					super.updateItem(name, empty);
					if (empty) {
						setText(null);
						setGraphic(null);
					} else {
						for (Photo p : Photos) {
							if (name.equals(p.path)) {
								imageView = new ImageView();
								String path = p.path;
								curr = new Image("file:///" + path);
								imageView.setX(20);
								imageView.setY(20);
								imageView.setFitWidth(100);
								imageView.setPreserveRatio(true);
								imageView.setImage(curr);
								setText(p.caption);
								setGraphic(imageView);
							}
						}
					}
				}
			});
		}

	}

	/**
	 * Sends user to search by Tag page
	 * 
	 * @param event event to be handled
	 * @throws IOException if error caught
	 * 
	 */
	public void searchByTag(ActionEvent event) throws IOException {

		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(getClass().getResource("/view/searchByTag.fxml"));
		Parent adminPage = fxmlLoader.load();
		Scene adminScene = new Scene(adminPage);
		searchByTagController controller = fxmlLoader.getController();
		controller.load(pictures, user, photosList);
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
	}

	/**
	 * Sends user to search by Date page
	 * 
	 * @param event event to be handled
	 * @throws IOException if error caught
	 *
	 * 
	 */
	public void searchByDate(ActionEvent event) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(getClass().getResource("/view/searchByDate.fxml"));
		Parent adminPage = fxmlLoader.load();
		Scene adminScene = new Scene(adminPage);
		searchByDateController controller = fxmlLoader.getController();
		controller.start(user);
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
	}

	/**
	 * Creates a new album containing the search results
	 * 
	 * 
	 *
	 */
	public void createAlbum() {
		userList.listInit();
		String s = "";
		Alert alertDel = new Alert(Alert.AlertType.ERROR);
		alertDel.setTitle("ERROR");
		alertDel.setHeaderText(null);
		if (photosList.isEmpty()) {

			alertDel.setContentText("The List is Empty! Please search for a photo");
			alertDel.showAndWait();
		} else {
			TextInputDialog dialog = new TextInputDialog();
			dialog.setTitle("New Album Name");
			dialog.setHeaderText("Enter new Album Title:");
			dialog.setContentText("Album name:");
			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()) {
				s = dialog.getEditor().getText();
				if (albumList.contains(s)) {
					alertDel.setContentText("Name already exists");
					alertDel.showAndWait();
					return;
				} else if (!s.equals("")) {
					user.addAlbum(s);
					Album temp = user.getAlbum(s);
					for (Photo p : photosList) {
						temp.addPhoto(p);
					}
					userList.updateUser(user);
				}
			}
		}
	}

	/**
	 * Goes back to the user page
	 * 
	 * @param event event to be handled
	 * @throws IOException if error caught
	 *
	 */
	public void back(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/albumList.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);
		userController controller = loader.getController();
		controller.start(user);

		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
	}

	/**
	 * Quits the program
	 * 
	 */
	public void quit() {
		System.exit(0);
	}

}
