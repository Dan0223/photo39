//Daniel Alvarado
//Al Manrique
package controller;

import java.awt.Container;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javafx.event.ActionEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.User;
import objects.UserList;
/**Controller that opens the album
 * 
 * 
 *
 */
public class AlbumOpenController {
	@FXML
	Label label;
	UserList userList = new UserList();
	@FXML
	ListView<String> listview = new ListView<String>();
	ImageView viewim;
	Album album;
	User user;
	@FXML
	ImageView imageView;
	ObservableList<String> items;
	Image curr;
/**
 * Initializes the userList and albumList and photoList and populates listView with tumbnail
 * @param album album to be opened
 * @param user user that contains the album
 */
	public void start(Album album, User user) {
		// System.out.println(album.albumName + " " + user.name);
		userList.listInit();
		this.album = album;
		this.user = user;
		label.setText(album.albumName);
		ArrayList<Photo> photosList = album.photoList;

		items = FXCollections.observableArrayList(getPath(photosList));
		listview.setItems(items);
		listview.setCellFactory(param -> new ListCell<String>() {

			private ImageView imageView = new ImageView();

			@Override
			/**populates the listView with Thumbnails 
			 * 
			 * @param name path of photo
			 * @param empty boolean
			 */
			public void updateItem(String name, boolean empty) {
				super.updateItem(name, empty);
				if (empty) {
					setText(null);
					setGraphic(null);
				} else {
					for (Photo p : photosList) {
						if (name.equals(p.path)) {
							imageView = new ImageView();
							String path = p.path;
							curr = new Image("file:///" + path);
							imageView.setX(20);
							imageView.setY(20);
							imageView.setFitWidth(100);
							imageView.setPreserveRatio(true);
							imageView.setImage(curr);
							setText(p.caption);
							setGraphic(imageView);
						}
					}
				}
			}
		});

	}
/**
 * Displays the editPhotoController
 * @param event event when this function is called
 * @throws IOException if error caught
 */
	@FXML
	public void display(ActionEvent event) throws IOException {
		try {
		FXMLLoader loader2 = new FXMLLoader();
		loader2.setLocation(getClass().getResource("/view/editPhoto.fxml"));
		Parent userPage1 = loader2.load();
		Scene userScene = new Scene(userPage1);
		editPhotoController controller = loader2.getController();
		//System.out.println(user.name + " " + album.albumName);
		controller.start(album, user);
		Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window1.setScene(userScene);
		window1.show();
		}catch(IndexOutOfBoundsException e) {
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("ERROR");
			alertDel.setHeaderText(null);
			alertDel.setContentText("No Photo");
			alertDel.showAndWait();
			return;
		}
		}
/**
 * deletes a photo in the Listview
 */
	public void delete() {

		String target = "";
		Alert alertDel = new Alert(Alert.AlertType.ERROR);
		alertDel.setTitle("ERROR");
		alertDel.setHeaderText(null);
		Alert confirmAlert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete the selected item?",
				ButtonType.YES, ButtonType.NO);
		if (album.photoList.isEmpty()) {
			alertDel.setContentText("The List is Empty! Please add a picture");
			alertDel.showAndWait();
			return;

		} else {
			confirmAlert.showAndWait();
			if (confirmAlert.getResult() == ButtonType.YES) {
				target = listview.getSelectionModel().getSelectedItem().trim();
				items.remove(target);
				album.deletePhoto(target);
				user.updateAlbum(album);
				userList.updateUser(user);

			}
		}
	}
	// listView.setCellFactory(param -> new ListCell<String>());
	/**
	 * adds photo in List view
	 * @param event event to be handled 
	 * @throws IOException if error caught
	 * 
	 */
	@FXML
	void addPhoto(ActionEvent event) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/customDialog.fxml"));
		Parent parent = fxmlLoader.load();
		// userList.listInit();
		customController controller = fxmlLoader.getController();
		controller.start(album, user);
		Scene scene = new Scene(parent);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.showAndWait();
		start(album, user);
	}
	/**returns a list of paths
	 * 
	 * @param pList the list of photos
	 * @return array list of strings
	 */
	public ArrayList<String> getPath(ArrayList<Photo> pList) {
		ArrayList<String> result = new ArrayList<String>();
		for (Photo p : pList) {
			result.add(p.path);

		}
		return result;
	}
/**
 * opens slideshow controller
 * @param event event to be handled
 * @throws IOException if error caught
 * 
 */
	@FXML
	public void slideShow(ActionEvent event) throws IOException {

		if (album.photoList.size() <= 0) {
			return;
		}

		FXMLLoader loader1 = new FXMLLoader();
		loader1.setLocation(getClass().getResource("/view/slideShow.fxml"));

		Parent userPage = loader1.load();
		Scene userScene = new Scene(userPage);
		slideShowController controller = loader1.getController();
		controller.start(album,user);
		Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window1.setScene(userScene);
		window1.show();

	}
	/**
	 * Opens the relocate page
	 * @param event event to be handled
	 * @throws IOException if error caught 
	 */
	
	public void relocate(ActionEvent event) throws IOException {
		if (user.albumList.size() < 2) {
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("ERROR");
			alertDel.setHeaderText(null);
			alertDel.setContentText("There is only 1 album in your list, please create more!");
			alertDel.showAndWait();

		} else if (items.isEmpty()) {
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("ERROR");
			alertDel.setHeaderText(null);
			alertDel.setContentText("There are no pictures to move");
			alertDel.showAndWait();
		} else {
			String target = "";
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/relocate.fxml"));
			Parent adminPage = loader.load();
			Scene adminScene = new Scene(adminPage);
			relocateController controller = loader.getController();
			target = listview.getSelectionModel().getSelectedItem().trim();
			controller.start(album, user, target);
			Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
			window.setScene(adminScene);
			window.show();
		}

	}
	/**
	 * Logs out to log in page
	 * @param event event to be handled
	 * @throws IOException if error caught 
	 */
	public void logOff(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);

		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
	}
	/**
	 * goes back to the album list
	 * @param event event to be handled
	 * @throws IOException if error caught
	 */
	public void back(ActionEvent event) throws IOException {
		FXMLLoader loader1 = new FXMLLoader();
		loader1.setLocation(getClass().getResource("/view/albumList.fxml"));
		// \Photos39Proj\src\view\albumList.fxml
		Parent albumList = loader1.load();
		Scene albumScene = new Scene(albumList);
		userController controller = loader1.getController();
		controller.start(user);
		Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window1.setScene(albumScene);
		window1.show();
	}
	/**
	 * closes project
	 * 
	 */
	public void quit() {
		System.exit(0);
	}

}
