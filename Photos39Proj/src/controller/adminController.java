//Daniel Alvarado
//Al Manrique
package controller;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

import javax.swing.JOptionPane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import objects.User;
import objects.UserList;
/**List of users that admin can delete or add
 * 
 *
 *  
 */
public class adminController implements Serializable {
	@FXML
	ListView<String> listView;
	@FXML
	TextField userBox;
	private static Scanner scn;
	private ObservableList<String> obsList;
	ArrayList<String> userListView = new ArrayList<String>();
	UserList userList = new UserList();
	ArrayList<User> newList = new ArrayList<>();
	
/**
 * starts up the login window
 */
	public void start() {
		userList.listInit(); //populates userList with what is in the user.sr file
		userListView = userList.getUsers(); //populate the ArrayList we will pass through the listView
		
		obsList = FXCollections.observableArrayList(userListView);
		FXCollections.sort(obsList, String.CASE_INSENSITIVE_ORDER);
		listView.setItems(obsList);
		listView.getSelectionModel().select(0);
	}

	public void quit() {
		System.exit(0);
	}


	/**Deletes user in the User List
	 * 
	 */
	public void deleteUser() {
		
		Alert alertDel = new Alert(AlertType.ERROR);
		alertDel.setHeaderText(null);
		if (obsList.isEmpty()||listView.getSelectionModel().getSelectedItem().trim().isBlank()
				|| listView.getSelectionModel().getSelectedItem().trim().isEmpty()) {
			alertDel.setContentText("The List is Empty!!!");
			alertDel.showAndWait();
			return;
		}else {
			String target = listView.getSelectionModel().getSelectedItem().trim();			
			obsList.remove(target);
			User toDelete = new User(target);
			userList.deleteUser(toDelete);
			
		}
		
		
	}
	/**deletes user in admin List
	 *
	 */
	public void addUser(){
		//Add new person to the UserList array and then add the UserList array to the file.
	User newUser = new User(userBox.getText().trim());	
	Alert alert = new Alert(AlertType.ERROR);
	alert.setHeaderText(null);
	if (userBox.getText().trim().isEmpty()) {
		alert.setTitle("Missing info");
		alert.setContentText("Field Is Empty");
		alert.showAndWait();
		return;
	}
	if(userList.addUser(newUser)) {
		//add it to the obsList and add it to the UserList overwrite the file with the new ArrayList.
		obsList.add(newUser.name);
		userBox.clear();
	}else {
		alert.setTitle("Duplicate");
		alert.setContentText("User already Exists.");
		alert.showAndWait();
		sortOBS();
		userBox.clear();
		return;
		
	}
		
	}

/**sorts observable list
 * 
 */
	public void sortOBS() {
		FXCollections.sort(obsList, String.CASE_INSENSITIVE_ORDER);
	}
	public void logOff(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);
	
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
	}

}