package controller;

import objects.Album;
import objects.User;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;
import javax.swing.JOptionPane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;
import objects.Album;
import objects.Photo;
import objects.Tag;
import objects.User;
import objects.UserList;
/**
 * Displays photo and all of the info 
 *
 *
 */
public class editPhotoController {
	UserList userList;
	Album album;
	User user;
	int position;
	ArrayList<Photo> photoList;
	Image curr;
	@FXML
	Label captionLabel;
	@FXML
	Label dateLabel;
	@FXML
	ImageView imageview;
	@FXML
	ListView<String> tagList;
	ObservableList<String> items;
	ArrayList<String> tags = new ArrayList<String>();
	String caption;
	String date;
/**
 * initializes the userList album and sets the Labels of the image
 * @param album passes in the current album
 * @param user passes in the current user
 */
	public void start(Album album, User user) {
		userList = new UserList();
		userList.listInit();
		this.album = album;
		this.user = user;

		position = 0;
		photoList = album.photoList;
		curr = new Image("file:///" + photoList.get(position).path);
		imageview.setImage(curr);
		imageview.setPreserveRatio(true);
		caption = photoList.get(position).caption;
		date = photoList.get(position).d.toString();
		if (!(caption.equals(null)) && caption != null) {
			captionLabel.setText(caption);
		}
		if (!(date.equals(null)) && date != null) {
			dateLabel.setText(date);
		}
		tags = getTags(photoList.get(position).tagList);
		items = FXCollections.observableArrayList(tags);
		FXCollections.sort(items, String.CASE_INSENSITIVE_ORDER);
		tagList.setItems(items);
		tagList.getSelectionModel().select(0);
//		ObservableList<String> items;
//		ListView<String> listview = new ListView<String>();
//		items = FXCollections.observableArrayList(getPath(photosList));
//		listview.setItems(items);
	}
/** Edits caption based on what user wants.
 * 
 *
 */
	public void edit() {
		String target = "";
		Alert alertDel = new Alert(Alert.AlertType.ERROR);
		alertDel.setTitle("ERROR");
		alertDel.setHeaderText(null);
		Alert confirmAlert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to edit the selected item?",
				ButtonType.YES, ButtonType.NO);
		if (album.photoList.isEmpty()) {
			alertDel.setContentText("The List is Empty! Please add a picture");
			alertDel.showAndWait();
			return;

		} else {
			confirmAlert.showAndWait();
			String s = "";
			if (confirmAlert.getResult() == ButtonType.YES) {
				TextInputDialog dialog = new TextInputDialog();
				dialog.setTitle("New Caption");
				dialog.setHeaderText("Enter new caption:");
				dialog.setContentText("Caption:");
				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()) {
					s = dialog.getEditor().getText();
					// might be an issue idk yet
					target = photoList.get(position).path;
					album.editCaption(target, s);
					captionLabel.setText(result.get());
					;
				}

				user.updateAlbum(album);
				userList.updateUser(user);

			} else {
				return;
			}
		}

	}
/**
 * adds a new tag to the photo
 * 
 */
	public void addTag() {
		String tType = "";
		String tName = "";
		Dialog<Pair<String, String>> dialog = new Dialog<>();

		dialog.setTitle("Add Tag");
		dialog.setHeaderText("Please Input Tag");
		ButtonType enter = new ButtonType("Add", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(enter, ButtonType.CANCEL);
		GridPane grid = new GridPane();
		grid.setHgap(15);
		grid.setVgap(15);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField tagType = new TextField();
		tagType.setPromptText("Type");
		TextField tagName = new TextField();
		tagName.setPromptText("Name");

		grid.add(new Label("Type"), 0, 0);
		grid.add(tagType, 1, 0);
		grid.add(new Label("Name"), 0, 1);
		grid.add(tagName, 1, 1);

		dialog.getDialogPane().setContent(grid);

		Optional<Pair<String, String>> result = dialog.showAndWait();
		Photo curr = photoList.get(position);
		if (result.isPresent()) {

			tType = tagType.getText().trim();
			tName = tagName.getText().trim();
			if (items.contains(tType + " , " + tName)) {
				Alert alertDel = new Alert(Alert.AlertType.ERROR);
				alertDel.setTitle("Duplicate Tags are not allowed.");
				alertDel.setHeaderText(null);
				alertDel.setContentText("Duplicate");
				alertDel.showAndWait();

			} else {
				if (tType.equals("") || tName.equals("")) {
					Alert alertDel = new Alert(Alert.AlertType.ERROR);
					alertDel.setTitle("Missing");
					alertDel.setHeaderText(null);
					alertDel.setContentText("Missing Info. Tag is in");
					alertDel.showAndWait();

				} else {
					curr.addTag(tType, tName);
					album.updatePhoto(curr);
					user.updateAlbum(album);
					userList.updateUser(user);
					items.add(tType + " , " + tName);
				}
			}
		}

	}
/**
 * deletes a tag from the photo
 */
	public void deleteTag() {

		String target = tagList.getSelectionModel().getSelectedItem();
		if (items.isEmpty() || target == null) {
			return;
		}
		String[] tag = target.split("[,]", 0);
		Photo curr = photoList.get(position);

		Tag toDelete = new Tag(tag[0].trim(), tag[1].trim());
		curr.deleteTag(toDelete);
		items.remove(target);
		album.updatePhoto(curr);
		user.updateAlbum(album);
		userList.updateUser(user);

	}
/**
 * Displays next photo in album
 */
	public void next() {
		int size = photoList.size();

		if (position + 1 > size - 1) {
			return;
		}
		position++;
		curr = new Image("file:///" + photoList.get(position).path);
		imageview.setImage(curr);
		imageview.setPreserveRatio(true);
		caption = photoList.get(position).caption;
		date = photoList.get(position).d.toString();

		if (!(caption.equals(null)) && caption != null) {
			captionLabel.setText(caption);
		}
		if (!(date.equals(null)) && date != null) {
			dateLabel.setText(date);
		}

		tags = getTags(photoList.get(position).tagList);
		items = FXCollections.observableArrayList(tags);
		FXCollections.sort(items, String.CASE_INSENSITIVE_ORDER);
		tagList.setItems(items);
		tagList.getSelectionModel().select(0);
	}
/**
 * Displays previous photo in album
 *  
 */
	public void prev() {

		if (position - 1 < 0) {
			return;
		}
		position--;

		curr = new Image("file:///" + photoList.get(position).path);
		imageview.setImage(curr);
		imageview.setPreserveRatio(true);
		caption = photoList.get(position).caption;
		date = photoList.get(position).d.toString();
		if (!(caption.equals(null)) && caption != null) {
			captionLabel.setText(caption);
		}
		if (!(date.equals(null)) && date != null) {
			dateLabel.setText(date);
		}
		tags = getTags(photoList.get(position).tagList);
		items = FXCollections.observableArrayList(tags);
		FXCollections.sort(items, String.CASE_INSENSITIVE_ORDER);
		tagList.setItems(items);
		tagList.getSelectionModel().select(0);
	}
/**
 * returns a List of tags that the photo has in a comma separated form
 * @param tagList List of Tags
 * @return ArrayList of tags that are of the photo
 */
	public ArrayList<String> getTags(ArrayList<Tag> tagList) {
		ArrayList<String> result = new ArrayList<String>();
		String line;
		for (Tag t : tagList) {
			line = t.type + " , " + t.name;
			result.add(line);
		}
		return result;
	}
	/**
	 * Returns a list of paths 
	 * 
	 * @param pList array of photos
	 * @return ArrayList of paths of photos
	 */

	public ArrayList<String> getPath(ArrayList<Photo> pList) {
		ArrayList<String> result = new ArrayList<String>();
		for (Photo p : pList) {
			result.add(p.path);
		}
		return result;
	}
	/**
	 * goes back to the album open page
	 * @param event to switch scenes
	 * @throws IOException if error caught
	 */
	public void back(ActionEvent event) throws IOException {
		String albumName = album.albumName;
		FXMLLoader loader1 = new FXMLLoader();
		loader1.setLocation(getClass().getResource("/view/albumOpen.fxml"));
		Parent userPage = loader1.load();
		Scene userScene = new Scene(userPage);
		AlbumOpenController controller = loader1.getController();
		controller.start(user.getAlbum(albumName), user);
		Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window1.setScene(userScene);
		window1.show();

	}
	/**
	 * Quits the application
	 */
	public void quit() {
		System.exit(0);
	}

}
