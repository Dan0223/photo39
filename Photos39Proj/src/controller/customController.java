package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.User;
import objects.UserList;
/***
 * Window to add new pic and tags 
 *
 */
public class customController {
	@FXML
	TextField caption;

	@FXML
	TextField tagName;
	@FXML
	TextField tagType;
	
	Album album;
	User user;
	UserList userList = new UserList();

	Label userLabel;
	ImageView imageView = new ImageView();

	// ListView<String> listView = new ListView<String>();
	// listView.setItems(Album.photoList);
	public void start(Album album, User user) {
		this.album = album;
		this.user = user;
	}
	/**
	 * adds a picture to the album
	 * @param event
	 */
	@FXML
	void addPic(ActionEvent event) {
		// Tag newTag = new Tag();
		
		// Album album = new Album();
		FileChooser chooser = new FileChooser();
		FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg",
				"*.gif");
		chooser.getExtensionFilters().add(filter);
		Stage stage = new Stage();

		try {

			userList.listInit();
			File file = chooser.showOpenDialog(stage);
			String path = file.getAbsolutePath();
			Photo newPhoto = new Photo(path);
			String tType = (tagType.getText().length() <= 0) ? "" : tagType.getText();
			String tName = (tagName.getText().length() <= 0) ? "" : tagName.getText();
			if (!tType.equals("") && !tName.equals("")) {
//				System.out.println(tType);
//				System.out.println(tName);
				newPhoto.addTag(tType, tName);				
			}

			newPhoto.caption = (caption.getText().length() <= 0) ? "" : caption.getText();

			album.addPhoto(newPhoto);
			user.updateAlbum(album);
			userList.updateUser(user);
			
		} catch (NullPointerException e) {
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("ERROR");
			alertDel.setHeaderText(null);
			alertDel.setContentText("No photo Selected");
			alertDel.showAndWait();

		}
		// newTag.tag = tagBox.getText();
		Node source = (Node) event.getSource();
		Stage stage1 = (Stage) source.getScene().getWindow();
		stage1.close();

	}

}
