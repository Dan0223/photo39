//Daniel Alvarado
//Al Manrique
package controller;

import java.awt.desktop.UserSessionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import javax.swing.JOptionPane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.User;
import objects.UserList;
import java.util.Optional;

/**
 * Displays the user controller where the user can create, edit, or delete
 * albums. The user can also access the search menu from there.
 *
 */

public class userController implements Serializable {

	@FXML
	ListView<String> albumListView;
	@FXML
	TextField searchBox;
	@FXML
	Label userLabel;
	private ObservableList<String> obsList;
	ArrayList<String> albumList = new ArrayList<String>();
	User user = new User(null);
	UserList userList = new UserList();

	/**
	 * Passes the user and all of his albums list that he has created
	 * 
	 * @param loginUser passes in the current user
	 */
	public void start(User loginUser) {
		this.user = loginUser;
		userLabel.setText(user.name);
		albumList = user.getAlbumNames();

		obsList = FXCollections.observableArrayList(albumList);
		FXCollections.sort(obsList, String.CASE_INSENSITIVE_ORDER);
		albumListView.setItems(obsList);
		albumListView.getSelectionModel().select(0);

	}

	/**
	 * Allows the user to quit the application at any time
	 */
	public void quit() {
		System.exit(0);
	}

	/**
	 * Allows the user to delete any album that he created
	 */
	public void delete() {
		userList.listInit();
		Alert alertDel = new Alert(AlertType.ERROR);
		alertDel.setHeaderText(null);
		if (obsList.isEmpty() || albumListView.getSelectionModel().getSelectedItem().trim().isBlank()
				|| albumListView.getSelectionModel().getSelectedItem().trim().isEmpty()) {
			alertDel.setContentText("The List is Empty");
			alertDel.showAndWait();
			return;
		} else {

			String target = albumListView.getSelectionModel().getSelectedItem().trim();

			obsList.remove(target);
			user.deleteAlbum(target);
			userList.updateUser(user);
		}

	}

	/**
	 * Allows the user to edit an album name that he chooses
	 */
	public void editAlbumName() {
		userList.listInit();
		String target = "";
		Alert alertDel = new Alert(Alert.AlertType.ERROR);
		alertDel.setTitle("ERROR");
		alertDel.setHeaderText(null);
		Alert confirmAlert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to edit the selected item?",
				ButtonType.YES, ButtonType.NO);
		if (obsList.isEmpty()) {
			alertDel.setContentText("The List is Empty! Please add an album");
			alertDel.showAndWait();
			return;

		} else {
			confirmAlert.showAndWait();
			String s = "";
			if (confirmAlert.getResult() == ButtonType.YES) {
				TextInputDialog dialog = new TextInputDialog();
				dialog.setTitle("New Album Name");
				dialog.setHeaderText("Enter new Album Title:");
				dialog.setContentText("Album name:");
				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()) {
					s = dialog.getEditor().getText();
					target = albumListView.getSelectionModel().getSelectedItem().trim();
					if (obsList.contains(s)) {
						alertDel.setContentText("Name already exists");
						alertDel.showAndWait();
						return;
					} else if (!s.equals("")) {
						obsList.remove(target);
						user.editAlbumName(target, s);
						userList.updateUser(user);
						obsList.add(s);
					}

				}

			} else {
				return;
			}
		}
	}

	/**
	 * Allows the user to access the search menu
	 * 
	 * @param event passes in the new scene
	 * @throws IOException if error caught
	 */
	public void search(ActionEvent event) throws IOException {

		if (obsList.size() == 0) {
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("ERROR");
			alertDel.setHeaderText(null);
			alertDel.setContentText("There are no albums, please create an album");
			alertDel.showAndWait();

		} else {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/mainSearch.fxml"));
			Parent adminPage = loader.load();
			Scene adminScene = new Scene(adminPage);
			mainSearchController controller = loader.getController();
			controller.start(user);
			Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
			window.setScene(adminScene);
			window.show();
		}

	}

	/**
	 * Allows the user to log out of his account safely
	 * 
	 * @param event passes in the new scene
	 * @throws IOException if error caught
	 */
	public void logOff(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/login.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
	}

	/**
	 * Creates a new album
	 */

	public void create() {
		userList.listInit();
		Alert alertDel = new Alert(Alert.AlertType.ERROR);
		alertDel.setTitle("ERROR");
		alertDel.setHeaderText(null);
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Album Name");
		dialog.setHeaderText("Enter album name:");
		dialog.setContentText("Album name:");
		String name = "";
		Optional<String> result = dialog.showAndWait();

		if (result.isPresent()) {
			name = result.get();
			if (albumList.contains(name)) {
				alertDel.setContentText("Name already exists");
				alertDel.showAndWait();
				return;
			} else if (!name.equals("")) {
				obsList.add(result.get());
				user.addAlbum(name);
				userList.updateUser(user);
			}
		}
	}
	/**
	 * Opens the selected album
	 * @param event passes in the new scene
	 * @throws IOException if error caught
	 */
	public void openAlbum(ActionEvent event) throws IOException {
		try {
			String albumName = albumListView.getSelectionModel().getSelectedItem().trim();
			if (obsList.isEmpty() || albumName.equals(null)) {
				Alert alertDel = new Alert(Alert.AlertType.ERROR);
				alertDel.setTitle("Empty");
				alertDel.setHeaderText(null);
				alertDel.setContentText("Album List is Empty. Please add an Album");
				alertDel.showAndWait();
				return;
			}

			FXMLLoader loader1 = new FXMLLoader();
			loader1.setLocation(getClass().getResource("/view/albumOpen.fxml"));
			Parent userPage = loader1.load();
			Scene userScene = new Scene(userPage);
			AlbumOpenController controller = loader1.getController();
			controller.start(user.getAlbum(albumName), user);
			Stage window1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
			window1.setScene(userScene);
			window1.show();
		} catch (NullPointerException e) {
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("No Selection");
			alertDel.setHeaderText(null);
			alertDel.setContentText("Album Not Selected");
			alertDel.showAndWait();
			return;
		}

	}
}
