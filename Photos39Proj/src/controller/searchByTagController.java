package controller;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import objects.Album;
import objects.User;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javafx.event.ActionEvent;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.Tag;
import objects.User;
import objects.UserList;
/**
 * Searches photos by Tag
 * 
 */
public class searchByTagController {
	@FXML
	TextField tag1;
	
	@FXML
	TextField value1;
	@FXML
	TextField tag2;
	@FXML
	TextField value2;
	ListView<String> listview = new ListView<String>();
	String stuff[] = {"AND", "OR"};
	@FXML
	ComboBox<String> comboBox;
	ArrayList<Photo> photosList;
	ArrayList<String> pictures;
	User user;
	ArrayList<String> albumList = new ArrayList<String>();
	/**
	 * Initializes user 
	 * @param pictures which is empty array list
	 * @param user passes in current user
	 * @param photosList passes in empty array list
	 */
	
	public void load(ArrayList<String>pictures, User user, ArrayList<Photo> photosList) {
		
		this.user = user;
		
		comboBox.getItems().addAll(stuff);
		
		//comboBox.setOnAction(event);
	}
	
	
	/**
	 * Sends the Main Search the photos that should be displayed in  the Main Search Page based on the tags
	 * @param event Event to be handled
	 * @throws IOException if error caught
	 */
	public void search(ActionEvent event) throws IOException {
		String tagType1 = tag1.getText();
		String tagValue1 = value1.getText();
		String tagType2 = tag2.getText();
		String tagValue2= value2.getText();
		boolean flag1 = true;
		boolean flag2 = true;
		//Photo tempPhoto;
		photosList= new ArrayList<Photo>();
		pictures= new ArrayList<String>();
		albumList = user.getAlbumNames();
		if(!tagType1.isEmpty() && !tagValue1.isEmpty() && tagType2.isEmpty() && tagValue2.isEmpty()) {
			for (String tempAlbumName : albumList) {
				Album temp1 = user.getAlbum(tempAlbumName);
				for (Photo p : temp1.photoList) {
					for(Tag temp: p.tagList) {
						
						if(temp.type.equals(tagType1) && temp.name.equals(tagValue1)) {
							pictures.add(p.path);
							photosList.add(p);
							
						}						
					}
				}
			}
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/mainSearch.fxml"));
			Parent adminPage = loader.load();
			Scene adminScene = new Scene(adminPage);
			mainSearchController controller = loader.getController();
			controller.loadPics(pictures, photosList, user);
			
			Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
			window.setScene(adminScene);
			window.show();
		}else if(!tagType1.isEmpty() && !tagValue1.isEmpty() && !tagType2.isEmpty() && !tagValue2.isEmpty()) {
			
			String temp = "";
			temp = (String) comboBox.getValue();
			if(temp==null|| temp.isBlank()||temp.isEmpty()) {
				Alert alertDel = new Alert(Alert.AlertType.ERROR);
				alertDel.setTitle("ERROR");
				alertDel.setHeaderText(null);
				alertDel.setContentText("Error! Missing fields");
				alertDel.showAndWait();
				return;
			}
			if(temp.equals("AND")) {
				for(String tempAlbumName: albumList) {
					Album temp1 = user.getAlbum(tempAlbumName);
					for(Photo p: temp1.photoList) {
						for(Tag newTemp1: p.tagList) {
							if(newTemp1.type.equals(tagType1) && newTemp1.name.equals(tagValue1)) {
								flag1 = true;
								break;
							}else {
								flag1= false;
							}
						}
						for(Tag newTemp2: p.tagList) {
							if(newTemp2.type.equals(tagType2) && newTemp2.name.equals(tagValue2)) {
								flag2 = true;
								break;
							}else {
								flag2 = false;
							}
						}
						if(flag1 && flag2) {
							Photo newPhoto = new Photo(p.path);
							
							newPhoto.caption= p.caption;
							newPhoto.tagList = getTags(p.tagList);
							
							pictures.add(p.path);
							photosList.add(newPhoto);
							
						}
					}
				}
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/mainSearch.fxml"));
				Parent adminPage = loader.load();
				Scene adminScene = new Scene(adminPage);
				mainSearchController controller = loader.getController();
				controller.loadPics(pictures, photosList, user);
				
				Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
				window.setScene(adminScene);
				window.show();
			
			}else if (temp.equals("OR")){
				for(String tempAlbumName: albumList) {
					Album temp1 = user.getAlbum(tempAlbumName);
					for(Photo p: temp1.photoList) {
						for(Tag newTemp1: p.tagList) {
							if(newTemp1.type.equals(tagType1) && newTemp1.name.equals(tagValue1)) {
								flag1 = true;
								break;
							}else {
								flag1= false;
							}
						}
						for(Tag newTemp2: p.tagList) {
							if(newTemp2.type.equals(tagType2) && newTemp2.name.equals(tagValue2)) {
								flag2 = true;
								break;
							}else {
								flag2 = false;
							}
						}
						if(flag1 || flag2) {
							pictures.add(p.path);
							photosList.add(p);
							
						}
					}
				}
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/mainSearch.fxml"));
				Parent adminPage = loader.load();
				Scene adminScene = new Scene(adminPage);
				mainSearchController controller = loader.getController();
				controller.loadPics(pictures, photosList, user);
				
				Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
				window.setScene(adminScene);
				window.show();
			}
		}else {
			Alert alertDel = new Alert(Alert.AlertType.ERROR);
			alertDel.setTitle("ERROR");
			alertDel.setHeaderText(null);
			alertDel.setContentText("Error! Missing fields");
			alertDel.showAndWait();
		}
		//Platform.exit();
		
	}
	/**
	 * Goes back to the Main Search page
	 * @param event passes in new scene
	 * @throws IOException if error caught
	 */
	public void back(ActionEvent event) throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/mainSearch.fxml"));
		Parent adminPage = loader.load();
		Scene adminScene = new Scene(adminPage);			
		mainSearchController controller = loader.getController();
		controller.start(user);
		
		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
		window.setScene(adminScene);
		window.show();
	}
	/**
	 * returns a List of tags in a  comma separated form
	 * @param source ArrayList of tags
	 * @return ArrayList returns an array list of tags
	 */
	public ArrayList<Tag> getTags(ArrayList<Tag> source) {
		ArrayList<Tag> result = new ArrayList<Tag>();
		for(Tag t: source) {
			
			result.add(t);
		}
		return result;
	}
	
	
	
}
