//Daniel Alvarado
//Al Manrique
package objects;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javafx.scene.image.Image;

/**
 * Photo object that saves the path and date of photos and takes in path in
 * parameter. Also contatians a Tag List
 * 
 *
 */
public class Photo implements Serializable {

	private static final long serialVersionUID = 1L;

	public Calendar c;

	public Date d;

	public String path;
	public String caption = "";
	public ArrayList<Tag> tagList;

	/**
	 * Photo constructor that initializes a the photos Tag List date ,and path
	 * 
	 * @param path name of photo
	 */
	public Photo(String path) {
		this.path = path;
		tagList = new ArrayList<Tag>();
		File file = new File(path);
		c = new GregorianCalendar();
		c.set(Calendar.MILLISECOND, 0);
		this.d = new Date(file.lastModified());

	}

	/**
	 * Adds a Tag to the tag List
	 * 
	 * @param tagType the tag Type
	 * @param tagName the tag Name
	 * 
	 */
	public void addTag(String tagType, String tagName) {
		// creates new tag and adds it to the tagList
		Tag t = new Tag(tagType, tagName);
		tagList.add(t);
	}

	/**
	 * Deletes a Tag in the tag List
	 * 
	 * @param target Tag that has to be deleted
	 * 
	 */
	public void deleteTag(Tag target) {
		for (Tag t : tagList) {
			if (t.type.equals(target.type) && t.name.equals(target.name)) {
				tagList.remove(t);
				break;
			}
		}

	}

	/**
	 * Checks if a tag is in the Tag List
	 * 
	 * @param tag Tag you are looking for
	 * @return boolean if the tag is in the photo
	 */
	public boolean contains(Tag tag) {
		for (Tag t : tagList) {
			if (t.name.equalsIgnoreCase(tag.name) && t.type.equalsIgnoreCase(tag.type)) {
				return true;
			}
		}
		return false;
	}

}
