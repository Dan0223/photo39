package objects;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;

import com.sun.prism.impl.Disposer.Target;

import java.io.EOFException;

public class UserList implements Serializable {

	/**
	 * List of Users that is initialized through a .ser file.
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ArrayList<User> userList = new ArrayList<>();

	// what this does is initializes the userList to equal the arrayList in the
	// user.ser file.
	@SuppressWarnings("unchecked")
	/**
	 * initializes the userList to equals to what is stored in the .ser file
	 * 
	 * 
	 */

	public void listInit() {

		try (FileInputStream fi = new FileInputStream("user.ser")) {
			try {

				ObjectInputStream os = new ObjectInputStream(fi);
				userList = (ArrayList<User>) os.readObject();
				os.close();
				fi.close();
			} catch (EOFException e) {
				userList = new ArrayList<>();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}

	/**
	 * deletes user
	 * 
	 * @param target user to be deleted
	 * 
	 */
	public void deleteUser(User target) {
		for (int x = 0; x < userList.size(); x++) {
			if (target.name.equals(userList.get(x).name)) {
				userList.remove(userList.get(x));// removes the user
				clearFile();// clears the user.ser file
				writeListToFile();// writes the updates version of the ArrayList.
			}
		}
	}

	/**
	 * adds User
	 * 
	 * @param newUser user to be added
	 * @return booleans if user is in the user list or not
	 */
	public boolean addUser(User newUser) {
		// adds User to the userList
		for (int x = 0; x < userList.size(); x++) {

			if (newUser.name.equals(userList.get(x).name)) {
				return false;
			}
		}

		userList.add(newUser); // add the new element
		clearFile();// clears the user.ser file
		writeListToFile();// writes the updates version of the ArrayList.
		return true;
	}

	/**
	 * gets a list of user names
	 * 
	 * @return ArrayList
	 */
	public ArrayList<String> getUsers() {
		ArrayList<String> listOfUsers = new ArrayList<>();
		for (User u : userList) {
			listOfUsers.add(u.name);
		}
		return listOfUsers;
	}

	private void clearFile() {

		try {
			PrintWriter writer;

			writer = new PrintWriter("user.ser");
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * writes the userList to the .ser file
	 */
	private void writeListToFile() {
		try (FileOutputStream fi = new FileOutputStream("user.ser")) {
			ObjectOutputStream os = new ObjectOutputStream(fi);
			os.writeObject(userList);
			os.close();
			fi.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * checks if a user is in the user list
	 * 
	 * @param name name of user you are trying to find
	 * @return if the name is in the user List
	 */
	public boolean contains(String name) {

		User curr = null;
		for (int x = 0; x < userList.size(); x++) {
			curr = userList.get(x);
			if (curr.name.equals(name)) {
				return true;
			}

		}
		return false;
	}

	/**
	 * Returns user
	 * 
	 * @param name name of the user you want to return
	 * @return User from the user list
	 */

	public User getUser(String name) {
		User curr = null;
		for (int x = 0; x < userList.size(); x++) {
			curr = userList.get(x);
			if (curr.name.equals(name)) {
				return curr;
			}
		}
		return null;
	}

	/**
	 * updates user in userList
	 * 
	 * @param u user to be updated
	 * @return boolean once the user has been updated
	 */
	public boolean updateUser(User u) {

		String target = u.name.trim();

		for (int x = 0; x < userList.size(); x++) {
			if (target.equals(userList.get(x).name.trim())) {

				userList.remove(userList.get(x));
				userList.add(u);
				clearFile();
				writeListToFile();
			}
		}
		return false;

	}

}
