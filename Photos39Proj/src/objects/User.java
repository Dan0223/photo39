//Daniel Alvarado
//Al Manrique
package objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User class that contains albums and the User name
 * 
 */
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	public String name;

	public ArrayList<Album> albumList = new ArrayList<Album>();

	/**
	 * User constructor that initializes the User name
	 * 
	 * @param name name of the User
	 * 
	 */
	public User(String name) {
		this.name = name;
	}

	/**
	 * Gets a List of album Names
	 * 
	 * @return ArrayList of album names
	 */
	public ArrayList<String> getAlbumNames() {
		ArrayList<String> listOfAlbum = new ArrayList<>();
		for (Album a : albumList) {

			listOfAlbum.add(a.albumName);

		}

		return listOfAlbum;
	}

	/**
	 * edits the Album name
	 * 
	 * @param name    old album name
	 * @param newName new album name
	 * 
	 */
	public void editAlbumName(String name, String newName) {
		for (Album a : albumList) {
			if (a.albumName.equals(name)) {
				a.albumName = newName;
			}
		}
	}

	/**
	 * adds new Album with the set name
	 * 
	 * @param name name of album
	 *
	 */
	public void addAlbum(String name) {

		Album a = new Album(name);
		albumList.add(a);
	}

	/**
	 * Deletes an album
	 * 
	 * @param album album to be deleted
	 * 
	 */
	public void deleteAlbum(String album) {
		for (Album a : albumList) {

			if (a.albumName.equals(album)) {

				albumList.remove(a);
				break;
			}
		}

	}

	/**
	 * Gets an album
	 * 
	 * @param aName name of the album user wants to get
	 * @return Album to get the entire album name of the album list
	 */
	public Album getAlbum(String aName) {
		for (Album a : albumList) {
			if (a.albumName.equals(aName)) {
				return a;
				// idk if this will cause a problem like it did before
			}
		}
		return null;
	}

	/**
	 * Updates Album
	 * 
	 * @param a album you want to update
	 */
	public void updateAlbum(Album a) {
		for (Album al : albumList) {
			if (al.albumName.equals(a.albumName)) {
				albumList.remove(al);
				albumList.add(a);
				break;
				// have to update user after calling this.
			}
		}
	}
}