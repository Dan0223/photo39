package objects;

import java.io.Serializable;

public class Tag implements Serializable {

	/**Tag used in photos
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String type;
	public String name;
/**Tag constructor used to initialize the type and name fields
 * 
 * @param type Tag Type
 * @param name Tag Name
 */
	public Tag (String type, String name) {
		this.type= type;
		this.name= name;
	}

}
