//Daniel Alvarado
//Al Manrique
package objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Album that has a photo list and name. Album is store in User object
 * 
 * 
 */
public class Album implements Serializable {

	private static final long serialVersionUID = 1L;
	public String albumName;
	public ArrayList<Photo> photoList;

	/**
	 * Initializes and Album of name albumName that contains a Photo List
	 * 
	 * @param albumName album Name
	 * 
	 */
	public Album(String albumName) {
		this.albumName = albumName;
		this.photoList = new ArrayList<Photo>();
	}

	/**
	 * Find photo and updates caption to new caption
	 * 
	 * @param path       The path of the photo you want to change
	 * @param newCaption new caption
	 * 
	 */
	public void editCaption(String path, String newCaption) {
		for (Photo photo : photoList) {
			if (photo.path.equals(path)) {
				photo.caption = newCaption;
			}
		}
	}

	/**
	 * Adds a photo to the photoList
	 * 
	 * @param photo photo you want to add
	 * 
	 */
	public void addPhoto(Photo photo) {
		photoList.add(photo);

	}

	/**
	 * Delete a photo to the photoList
	 * 
	 * @param target path of the photo you want to delete
	 * 
	 */
	public void deletePhoto(String target) {
		for (Photo p : photoList) {
			if (p.path.equals(target)) {
				photoList.remove(p);
				break;
			}
		}
	}

	/**
	 * Updates the photo in photoList
	 * 
	 * @param p photo you want to update
	 * 
	 */
	public void updatePhoto(Photo p) {
		for (Photo ph : photoList) {
			if (ph.path.equals(p.path)) {
				photoList.remove(ph);
				photoList.add(p);
				break;
				// have to update album after calling this.
			}
		}
	}

	/**
	 * Gets a photo to the photoList
	 * 
	 * @param s path of the photo you want to get
	 * @return returns the photo
	 */
	public Photo getPhoto(String s) {
		for (Photo ph : photoList) {
			if (ph.path.equals(s)) {
				return ph;
			}
		}
		return null;
	}
}
